(defproject com.sbnl.sa.layout "0.1.0-SNAPSHOT"
    :description "Contains code for a layout tool to support drawing in the sa project"
    :url "http://example.com/FIXME"
    :license {:name "Eclipse Public License"
              :url "http://www.eclipse.org/legal/epl-v10.html"}

    :plugins [];[lein-expectations "0.0.8"]
              ;[lein-autoexpect   "1.9.0"]
              ;[lein-tools-deps   "0.4.3"]]

    ;:middleware [lein-tools-deps.plugin/resolve-dependencies-with-deps-edn]

    ;:lein-tools-deps/config {:config-files [:project]}

    :source-paths ["src/clj" "src/cljs" "src/cljc"]

    :test-paths   ["test/clj"])
