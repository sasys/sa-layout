(ns sa-layout.fixed-grid.test-fixed-grid
  "
    Test cases for the fixed grid impl
  "
  (:require [clojure.test                    :refer [is deftest]]
            [sa-layout.fixed-grid.fixed-grid :refer [split-grid, import-positions simple-factor]]))

(def test-model-1 {:sadf/id "0"
                   :sadf/children [{:sadf/id "1"
                                    :sadf/children []}
                                   {:sadf/id "2"
                                    :sadf/children []}
                                   {:sadf/id "3"
                                    :sadf/children []}
                                   {:sadf/id "4"
                                    :sadf/children []}]})



(deftest fixed-gridlayout
  (is (= [[200 150]
          [400 150]
          [600 150]
          [200 300]
          [400 300]
          [600 300]
          [200 450]
          [400 450]
          [600 450]]
         (split-grid [800 600]))  "Use the first level of the mode by default"))

(deftest test-simple-factor
  (is (= [4 4] (simple-factor 12)))
  (is (= [10 10] (simple-factor 100))))

;In this test we check what happens when the number of items on a layer exceeds the
;initial capacity to generate the grid.
(deftest fixed-gridlayout-bigger-grid
  (is (= [[160 120]
          [320 120]
          [480 120]
          [640 120]
          [160 240]
          [320 240]
          [480 240]
          [640 240]
          [160 360]
          [320 360]
          [480 360]
          [640 360]
          [160 480]
          [320 480]
          [480 480]
          [640 480]]
         (split-grid 12 [800 600]))  "Build a grid for twelve items")
  (is (= [[200 150]
          [400 150]
          [600 150]
          [200 300]
          [400 300]
          [600 300]
          [200 450]
          [400 450]
          [600 450]] (split-grid 5 [800 600]))))

(deftest generate_layout
  (is (= {(keyword "sadf" "0") [400 300],
          (keyword "sadf" "1") [267 200],
          (keyword "sadf" "2") [534 200],
          (keyword "sadf" "3") [267 400],
          (keyword "sadf" "4") [534 400]} (import-positions test-model-1 "sadf" [800 600]))))
