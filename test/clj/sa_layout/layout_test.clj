(ns sa-layout.layout-test
  "
    Test cases for the jvm based layout factory operations
  "
  (:require [clojure.test                :refer [is deftest]]
            [sa-layout.layout            :refer [layout generate-layout]]
            [sa-layout.layout-factory    :refer [get-layout]]
            [sa-layout.fixed-grid.test-fixed-grid]
            [pjstadig.humane-test-output :as pj]))

;For some reason this is not working in my .profiles.clj
(pj/activate!)

(def sample-1
  {:id           "1"
   :title        "Context"
   :description  "The context diagram"
   :children (list {:id          "1.1"
                    :title       "Do proc A"
                    :description "This describes the process of making As"
                    :children (list {:id          "1.1.1"
                                     :title       " Do proc A.A"
                                     :description "This describes the process of making A.As"
                                     :children (list)})}
                   {:id          "1.2"
                    :title       "Do proc B"
                    :description "This describes the process of making Bs"
                    :children (list {:id          "1.2.1"
                                     :title       " Do proc B.A"
                                     :description "This describes the process of making B.As"
                                     :children (list)})}
                   {:id          "1.3"
                    :title       "Do proc C"
                    :description "This describes the process of making Cs"
                    :children (list {:id          "1.3.1"
                                     :title       " Do proc C.A"
                                     :description "This describes the process of making C.As"
                                     :children (list)}
                                    {:id          "1.3.2"
                                     :title       " Do proc C.B"
                                     :description "This describes the process of making C.Bs"
                                     :children (list)}
                                    {:id          "1.3.3"
                                     :title       " Do proc C.C"
                                     :description "This describes the process of making C.Cs"
                                     :children (list)})}
                   {:id           "1.4"
                    :title        "Do proc D"
                    :description  "Do stuf to make D work!!"
                    :children     '()})})


(def sample-2
  {:sadf/id           "1"
   :sadf/title        "Context"
   :sadf/description  "The context diagram"
   :sadf/children (list {:sadf/id          "1.1"
                         :sadf/title       "Do proc A"
                         :sadf/description "This describes the process of making As"
                         :sadf/children (list {:sadf/id          "1.1.1"
                                               :sadf/title       " Do proc A.A"
                                               :sadf/description "This describes the process of making A.As"
                                               :sadf/children (list)})}
                        {:sadf/id          "1.2"
                         :sadf/title       "Do proc B"
                         :sadf/description "This describes the process of making Bs"
                         :sadf/children (list {:sadf/id          "1.2.1"
                                               :sadf/title       " Do proc B.A"
                                               :sadf/description "This describes the process of making B.As"
                                               :sadf/children (list)})}
                        {:sadf/id          "1.3"
                         :sadf/title       "Do proc C"
                         :sadf/description "This describes the process of making Cs"
                         :sadf/children (list {:sadf/id          "1.3.1"
                                               :sadf/title       " Do proc C.A"
                                               :sadf/description "This describes the process of making C.As"
                                               :sadf/children (list)}
                                              {:sadf/id          "1.3.2"
                                               :sadf/title       " Do proc C.B"
                                               :sadf/description "This describes the process of making C.Bs"
                                               :sadf/children (list)}
                                              {:sadf/id          "1.3.3"
                                               :sadf/title       " Do proc C.C"
                                               :sadf/children (list)
                                               :sadf/description "This describes the process of making C.Cs"})}
                        {:sadf/id           "1.4"
                         :sadf/title        "Do proc D"
                         :sadf/description  "Do stuf to make D work!!"
                         :sadf/children     (list)})})


(deftest fixed-layout
  (let [lay (get-layout :sa-layout.layout-factory/fixed-grid)]
   (is (= {(keyword "sa-draw.events" "1.1") [100 100]
           (keyword "sa-draw.events" "1.2") [200 100]
           (keyword "sa-draw.events" "1.3") [300 100]
           (keyword "sa-draw.events" "1.4") [100 200]}
         (layout lay sample-1 [400 400])) "Use the first level of the model by default")

   (is (= {(keyword "sa-draw.events" "1.1") [100 100]
           (keyword "sa-draw.events" "1.2") [200 100]
           (keyword "sa-draw.events" "1.3") [300 100]
           (keyword "sa-draw.events" "1.4") [100 200]}
         (layout lay  "1" sample-1 [400 400])) "Specify the first level by default")

   (is (= {(keyword "sa-draw.events" "1.3.1") [100 100]
           (keyword "sa-draw.events" "1.3.2") [200 100]
           (keyword "sa-draw.events" "1.3.3") [300 100]}
         (layout lay  "1.3" sample-1 [400 400])))) "Specify a lower level")

; (deftest gen-layout
;   (let [namespace "sadf"
;         lay (get-layout :sa-layout.layout-factory/fixed-grid)]
;     (is (= {(keyword namespace "1")     [200 200]
;             (keyword namespace "1.1")   [100 100]
;             (keyword namespace "1.2")   [200 100]
;             (keyword namespace "1.3")   [300 100]
;             (keyword namespace "1.4")   [100 400]
;             (keyword namespace "1.1.1") [100 100]
;             (keyword namespace "1.2.1") [100 100]
;             (keyword namespace "1.3.1") [100 100]
;             (keyword namespace "1.3.2") [200 100]
;             (keyword namespace "1.3.3") [100 300]} (generate-layout lay sample-2 "sadf" [400 400])))))

(defn -main []
  (clojure.test/run-tests 'sa-layout.layout-test
                          'sa-layout.fixed-grid.test-fixed-grid))
