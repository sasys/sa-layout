(ns sa-layout.dot.dot1
 (:require [sa-layout.layout :refer [Layout]]))

(def dot
  (reify Layout
    (layout [this model [mx my]] {})
    (layout [this node-id model [mx my]] {})
    (layout [this name-space node-id model [mx my]] {})))
