(ns sa-layout.layout
  "Provides a protocol to support different layout tools.
   The intent here is to allow a model to be input and a layout to
   be generated where the layout provides a center location for every
   proc.

   The model can be any node or hierarchy of nodes.
   The response will be a map keyed on proc id with an associated
   vector [xy]")

(defprotocol Layout
  "
   Protocol describes operations to layout a model.
   node-id is the optional string id of the node to layout like \"1.2\".  In practice this means
   the children of the node with given id.
   max is a vector like [mx my]containing the maximum x and the maximum y
   of the canvas.
  "
  #?(:cljs (layout [this model [mx my]]
                   [this node-id model [mx my]]
                   [this name-space node-id model [mx my]])
     :clj  (layout [this model [mx my]]
                   [this node-id model [mx my]]
                   [this name-space node-id model [mx my]]))

  #?(:cljs (generate-layout [this procs namespace [mx my]])
     :clj  (generate-layout [this procs namespace [mx my]])))
