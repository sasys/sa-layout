(ns sa-layout.fixed-grid.fixed-grid
  "
    Fixed grid layout assumes a 3 by 3 grid and just lays out in sequence
    from left to right and top to bottom
  "
  (:require [sa-layout.layout :refer [Layout]]))

(def key-domain "sa-draw.events")

;The default number of items to represent in a grid
(def default-count 9)

;;grid-coulmns and grid-rows defines the horzontal and vertical layout pattern
;;for up to 9 nodes patter layer
(def grid-columns 3)
(def grid-rows 3)

(defn simple-factor
  "
      Express the given number n near to x * y as integers.
      Simple factor just uses a sqrt function.
  "
  [n]
  (if (<= n 1)
    [1 1]
    (let [f (int (Math/ceil (Math/sqrt n)))]
      [f f])))

(defn split-grid
  "
    Splits the x and the y into four spaces with 3 points in the z and the y
    and forms coordinate pairs.
  "
  ([[x y]]
   (split-grid default-count [x y]))
  ([count [x y]]
   (let [[grid-rows grid-columns] (simple-factor count)
         dx (/ x (+ grid-rows 1))
         dy (/ y (+ grid-columns 1))]
     (apply concat (for [yc (range 1 (+ grid-rows +1))]
                    (for [xc (range 1 (+ grid-columns +1))]
                     [(int (Math/ceil (* dx xc))) (int (Math/ceil (* dy yc)))]))))))

(defn make-grid
  "
    Given a collection of ids, makes a layout map from id to position.
    [mx my] the max x and max y on the area to be layed out.
  "
  [ids namespace [mx my]]
  (into {} (map (fn [%1 %2] {(keyword namespace %1) %2}) ids (split-grid (count ids) [mx my]))))


(defn find-node
  "Given a proc id find a node or nil.  Searches the complete tree and returns
   the first match."
  [proc-id node]
  (into {} (if (= (:id node) proc-id)
            node
            (mapcat #(find-node proc-id %) (:children node)))))


(defn find-node-in-tree
    "Given a proc id find a node or nil.  Searches the complete tree and returns
     the first match."
    [proc-id node name-space]
    (let [kid (keyword name-space "id")
          chil (keyword name-space "children")]
        (if (= (kid node) proc-id)
          node
          (into {}
            (for [cnode (chil node)]
              (find-node-in-tree proc-id cnode name-space))))))


(defn do-layout
  "Lays out the children of the given model."
  ([model [mx my]]
   (let [grid-pairs (split-grid [mx my])
         ids        (map :id (:children model))]
      (into {} (map (fn [id gp] {(keyword key-domain id) gp}) ids grid-pairs))))

  ([node-id model [mx my]]
   (let [node (find-node node-id model)]
      (do-layout node [mx my])))

  ([name-space node-id model [mx my]]
   (let [node (find-node-in-tree node-id (get-in model [:model  :data]) name-space)
         grid-pairs (split-grid [mx my])
         ids        (map (fn [node]((keyword name-space "id") node)) ((keyword name-space "children") node))]
      (into {} (map (fn [id gp] {(keyword name-space id) gp}) ids grid-pairs)))))


; (defn find-node-ids [node namespace]
;   "Given a model (or a node) make a list of all the ids of the children within that node"
;   (let [name-space-children (keyword namespace "children")
;         name-space-id (keyword namespace "id")]
;     (map name-space-id (name-space-children node))))


(defn import-positions
  "
    Generates positions for nodes in each layer of a model.
    For example, when importing a markdown document, the model has no positions so
    we set out a simple grid on each subtree.

    procs is the process model, ie the top level.
    namespace is the namespace!!
    [mx my] is the max x and max y in pixels.
  "
  ([parent namespace [mx my]]
   (if (some? (seq ((keyword namespace "children") parent)))
     (merge {(keyword namespace ((keyword namespace "id") parent)) [(/ mx 2) (/ my 2)]}
            (import-positions {} ((keyword namespace "children") parent) namespace (keyword namespace "id") (keyword namespace "children") [mx my]))
     {(keyword namespace ((keyword namespace "id") parent)) [(/ mx 2) (/ my 2)]}))

  ([locations procs namespace kid chil [mx my]]
   (let [locs (merge locations (make-grid (map #(kid %) procs) namespace [mx my]))]
     (if (some? (seq procs))
       (merge locs (into {} (map (fn [proc] (import-positions locs (chil proc) namespace kid chil [mx my])) procs)))
       locations))))


(defn cids
  [procs namespace]
  (map #((keyword namespace "id") %) procs))



#?(:clj (def grid
         (reify Layout
          (layout [this model [mx my]]
           (do-layout model [mx my]))

          (layout [this node-id model [mx my]]
            (do-layout node-id model [mx my]))

          (layout [this name-space node-id model [mx my]])

          (generate-layout [this parent namespace [mx my]]
            (import-positions parent namespace [mx my]))))

   :cljs (deftype Grid []
          Layout
           (layout [this model [mx my]]
            (do-layout model [mx my]))

           (layout [this node-id model [mx my]]
            (do-layout node-id model [mx my]))

           (layout [this name-space node-id model [mx my]]
            (do-layout name-space node-id model [mx my]))

           (generate-layout [this parent namespace [mx my]]
             (import-positions parent namespace [mx my]))))
