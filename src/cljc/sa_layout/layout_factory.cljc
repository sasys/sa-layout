(ns sa-layout.layout-factory
  "Provides a protocol to support different layout tools.
   The intent here is to allow a model to be input and a layout to
   be generated."
  (:require #?(:cljs [sa-layout.fixed-grid.fixed-grid :refer [Grid]]
               :clj  [sa-layout.fixed-grid.fixed-grid :refer [grid]])
            #?(:clj  [sa-layout.dot.dot1              :refer [dot]])))


;A hierarchy of layout algs
(def layout-hierarchy (-> (make-hierarchy)
                          (derive ::fixed-grid ::layout) ;a simple fixed grid layout
                          #?(:clj (derive ::dot1 ::layout))))     ;a dot based layout
  ;                        #?(:cljs (derive ::rest       ::layout))))) ;a remote layout



(defmulti get-layout (fn [mode] mode) :hierarchy #'layout-hierarchy)

#?(:clj (defmethod get-layout ::fixed-grid [mode]
              grid)
   :cljs (defmethod get-layout ::fixed-grid [mode]
              (Grid.)))

#?(:clj (defmethod get-layout ::dot1 [mode]
         dot))
